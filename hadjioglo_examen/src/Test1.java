import java.sql.*;
import java.util.Scanner;

public class Test1 {

    public static class SQLConnect {
        public static void main(String[] args) {
            String uname = "root";
            String pass = "admin123";
            String url = "jdbc:mysql://localhost:3306/db4";

            Scanner obj1 = new Scanner(System.in);
            System.out.println("Doriti sa inregistrati un student nou? (Da/Nu)");
            String choice = obj1.next();

            // check if user wants to introduce new Student
            checkChoice(choice);

            System.out.println("Introducere nume student: ");
            String name = obj1.next();

            System.out.println("Introducere prenume student: ");
            String familyName = obj1.next();

            System.out.println("Introducere grupa student: ");
            String group = obj1.next();

            System.out.println("Introducere IDNP student: ");
            long idnp = obj1.nextLong();

            System.out.println("Introducere nota test 1: ");
            int nota1 = obj1.nextInt();

            System.out.println("Introducere nota test 2: ");
            int nota2 = obj1.nextInt();

            System.out.println("Introducere nota examen: ");
            double notaExamen = obj1.nextDouble();

            System.out.println("Doriti sa aflati media aritmetica? (Da/Nu)");
            String choiceMedie = obj1.next();
            //calculate average and check if user want to get average mark
            checkChoiceMedie(choiceMedie, calculateMedia(nota1, nota2, notaExamen));

            System.out.println("Doriti sa afisati toata informatia despre student? (Da/Nu)");
            //insert all information to Database
            insertInfo(name, familyName,group,idnp,nota1,nota2,notaExamen,calculateMedia(nota1, nota2, notaExamen),url,uname,pass);
            String choiceInfo = obj1.next();
            //check if uesr wants to see all information from DB
            checkChoiceInfo(choiceInfo);


            try {
                Connection con = DriverManager.getConnection(url, uname, pass);
                Statement st = con.createStatement();
                ResultSet resultOfMyQuery = st.executeQuery("select * from Students where idnp = '"+ idnp+"'");
                while (resultOfMyQuery.next()) {
                    System.out.print(resultOfMyQuery.getString(1) + " | ");
                    System.out.print(resultOfMyQuery.getString(2) + " | ");
                    System.out.print(resultOfMyQuery.getString(3) + " | ");
                    System.out.print(resultOfMyQuery.getString(4) + " | ");
                    System.out.print(resultOfMyQuery.getString(5) + " | ");
                    System.out.print(resultOfMyQuery.getString(6) + " | ");
                    System.out.print(resultOfMyQuery.getString(7) + " | ");
                    System.out.println(resultOfMyQuery.getString(8) + " | ");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        public static void checkChoice(String choice) {
            String formattedChoice = choice.toLowerCase();
            if (formattedChoice.equals("da")) {
            } else {
                System.out.println("iesire din program ...");
                System.exit(0);
            }
        }
        private static double calculateMedia(int nota1, int nota2, double notaExamen) {
            double sum, medie;
            sum = (nota1 + nota2) / 2.0;
            medie = (sum + notaExamen) / 2.0;
            return medie;
        }
        public static void checkChoiceMedie(String choice, double medie) {
            String formattedChoice = choice.toLowerCase();
            if (formattedChoice.equals("da")) {
                System.out.println("Media aritmetica = " + medie);
            } else {
                System.out.println("iesire din program ...");
                System.exit(0);
            }
        }
        public static void checkChoiceInfo(String choice) {
            String formattedChoice = choice.toLowerCase();
            if (formattedChoice.equals("da")) {
            } else {
                System.out.println("iesire din program ...");
                System.exit(0);
            }
        }
        public static void insertInfo(String name, String family_name, String group, long idnp, int nota1, int nota2, double notaExamen, double medie, String url, String uname, String pass){
            try {
                Connection con = DriverManager.getConnection(url, uname, pass);
                Statement st = con.createStatement();
                st.executeUpdate("insert into Students(idnp, student_name, family_name, group_name, nota1, nota2, nota_examen, medie) values(" + idnp + ",'" + name + "', '" + family_name + "','" + group + "'," + nota1 + "," + nota2 + "," + notaExamen + "," + medie + ")");
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

}
